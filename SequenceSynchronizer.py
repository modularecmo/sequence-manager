# import json
# from Modules import *
# from SequenceManager import *
from Setup import *

class SequenceSynchronizer():

    def __init__(self, path, repeat = True):
        self.path = path
        self.sequenceStream = {}
        self.repeat = repeat
        self.sequenceManager = SequenceManager()
        self.firstTime = True
        self.currentJobIndex = 0
        self.currentJobIndexBackup = 0
        self.doIStop = True

    def setCurrentJobIndex(self, index, path):
        print("setCurrentJobIndex path:", path, "to", index)
        if path == "/main":
            self.currentJobIndex = index
            database.child("sequence/currentJobIndex").set(self.currentJobIndex)
        elif "/waitForAction/successQueue" in path or "/waitForAction/failureQueue" in path:
            print("Setting wait for action job index to", index)
            self.currentJobIndex = index
            database.child("sequence/" + path + "/currentJobIndex").set(self.currentJobIndex)

    def getSequence(self) -> {}:
        # Retrieve sequence JSON object
        _sequence = database.child(self.path).get().val()
        return _sequence

    def saveAsJSON(self, dictionry):
        with open('data/sequence.json', 'w') as fp:
            json.dump(dictionry, fp)

    def sync(self, json):
        if json is not None:
            print("Analyzing JSON and populating sequence manager...")

            if self.repeat is False and self.firstTime:
                self.sequenceStream.close()
                print("Repeat disabled.")
                self.firstTime = False

            if (json["path"] == "/" or json["path"] == "/0") and json["event"] == "put":
                sequence = self.getSequence()
                print("Main Sequence:", sequence)
                if sequence is not None:
                    self.saveAsJSON(sequence)
                    database.child("sequence/connected").set(True)
                    self.createAndRunSequenceFrom(sequence, "/main")
                else:
                    print("Sequence is empty.")
            print("Updated JSON:", json, "\n")


    def createAndRunSequenceFrom(self, json, path):
        self.sequenceManager = SequenceManager()
        self.currentJobIndex = 0
        for index, job in enumerate(json):
            print(index, ":", job)
            for key, value in job.items():
                durationPath = path + "/" + str(index) + "/" + key + "/duration"
                print("durationPath:", durationPath)
                if key == "stop":
                    print("Stop Simulation")
                    stopSimulation = StopSimulation()

                    def stop():
                        stopSimulation.stop()
                        self.setCurrentJobIndex(index = self.currentJobIndex + 1, path = path)

                    self.sequenceManager.schedule(1, stopSimulation.execute, onFinish = stop)
                elif key == "changeParameters":
                    print("Change Parameters")
                    parallelModule = Module.moduleFromJSON(value["parallelModule"]) if (
                    "parallelModule" in value) else None
                    changeParameters = ChangeParameters(value, parallelModule)

                    def stop():
                        # changeParameters.stop()
                        self.setCurrentJobIndex(index = self.currentJobIndex + 1, path = path)

                    self.sequenceManager.schedule(1, changeParameters.execute, onFinish = changeParameters.stop, updateHandler = stop)
                elif key == "displayMessage":
                    print("Display Message")
                    parallelModule = Module.moduleFromJSON(value["parallelModule"]) if (
                        "parallelModule" in value) else None
                    message = DisplayMessage(message = value["message"], barMessage = value["barMessage"], alarm = value["alarm"], priority = value["priority"], duration = value["duration"], parallelModule = parallelModule)
                    jobDuration = value["duration"] if (value["duration"] != "nil") else None

                    def stop():
                        #message.stop()
                        self.setCurrentJobIndex(index = self.currentJobIndex + 1, path = path)

                    self.sequenceManager.schedule(jobDuration, message.execute, signalPath = durationPath, onFinish = message.stop, updateHandler = stop)
                elif key == "waitForAction":
                    print("Wait For Action")
                    successQueue = value["successQueue"] if "successQueue" in value else None
                    failureQueue = value["failureQueue"] if "failureQueue" in value else None
                    wait = WaitForAction(index, synchronizer, successQueue, failureQueue)
                    def stop():
                        self.setCurrentJobIndex(index = self.currentJobIndexBackup + 1, path = path)
                    self.sequenceManager.schedule(1, wait.execute, onFinish = stop)
                elif key == "delay":
                    print("Delay")
                    parallelModule = Module.moduleFromJSON(value["parallelModule"]) if (
                        "parallelModule" in value) else None
                    delay = Delay(value["duration"], parallelModule)
                    jobDuration = value["duration"] if (value["duration"] != "nil") else None

                    def stop():
                        #delay.stop()
                        self.setCurrentJobIndex(index = self.currentJobIndex + 1, path = path)

                    self.sequenceManager.schedule(jobDuration, delay.execute, onFinish = delay.stop, signalPath = durationPath, updateHandler = stop)
                elif key == "bleeding":
                    print("Bleeding")
                    parallelModule = Module.moduleFromJSON(value["parallelModule"]) if ("parallelModule" in value) else None
                    bleeding = Bleeding(duration = value["duration"], bleedFlowRate=value["bleedFlow"], parallelModule = parallelModule)
                    jobDuration = value["duration"] if (value["duration"] != "nil") else None

                    def stop():
                        #bleeding.stop()
                        self.setCurrentJobIndex(index = self.currentJobIndex + 1, path = path)

                    self.sequenceManager.schedule(jobDuration, bleeding.execute, signalPath = durationPath, onFinish = bleeding.stop, updateHandler = stop)
                elif key == "lineShattering":
                    print("Line Shattering")
                    parallelModule = Module.moduleFromJSON(value["parallelModule"]) if (
                        "parallelModule" in value) else None
                    shatter = LineShattering(value["duration"], value["mode"], value["center"], parallelModule)
                    jobDuration = value["duration"] if (value["duration"] != "nil") else None
                    if value["center"] == True:
                        print("Centering line shattering...");
                        database.child(shatter.path).set(1)

                    def stop():
                        self.setCurrentJobIndex(index = self.currentJobIndex + 1, path = path)

                    self.sequenceManager.schedule(jobDuration, shatter.execute, signalPath = durationPath, onFinish = shatter.stop, updateHandler = stop)
                elif key == "powerDisconnection":
                    print("Power Disconnection")
                    parallelModule = Module.moduleFromJSON(value["parallelModule"]) if (
                        "parallelModule" in value) else None
                    batteryLevel = value["batteryLevel"] if (value["batteryLevel"] != "nil") else None
                    fullBatteryLife = value["fullBatteryLife"] if (value["fullBatteryLife"] != None) else None
                    off = PowerDisconnection(batteryLevel = batteryLevel, fullBatteryLife = fullBatteryLife, duration = value["duration"], parallelModule = parallelModule)
                    jobDuration = value["duration"] if (value["duration"] != "nil") else None

                    def stop():
                        #off.stop()
                        self.setCurrentJobIndex(index = self.currentJobIndex + 1, path = path)

                    self.sequenceManager.schedule(jobDuration, off.execute, signalPath = durationPath, onFinish = off.stop, updateHandler = stop)
                elif key == "oxygenatorNoise":
                    print("Oxygenator Noise")
                    parallelModule = Module.moduleFromJSON(value["parallelModule"]) if (
                        "parallelModule" in value) else None
                    air = OxygenatorNoise(value["volume"], value["duration"], parallelModule)
                    jobDuration = value["duration"] if (value["duration"] != "nil") else None

                    def stop():
                        #air.stop()
                        self.setCurrentJobIndex(index = self.currentJobIndex + 1, path = path)

                    self.sequenceManager.schedule(jobDuration, air.execute, signalPath = durationPath, onFinish = air.stop, updateHandler = stop)
                elif key == "deoxygenation":
                    print("Deoxygenation")
                    parallelModule = Module.moduleFromJSON(value["parallelModule"]) if (
                        "parallelModule" in value) else None
                    deox = Deoxygenation(value["duration"], parallelModule)
                    jobDuration = value["duration"] if (value["duration"] != "nil") else None

                    def stop():
                        #deox.stop()
                        self.setCurrentJobIndex(index = self.currentJobIndex + 1, path = path)

                    self.sequenceManager.schedule(jobDuration, deox.execute, signalPath = durationPath, onFinish = deox.stop, updateHandler = stop)
                elif key == "clotting":
                    print("Clotting")
                    parallelModule = Module.moduleFromJSON(value["parallelModule"]) if (
                        "parallelModule" in value) else None
                    clotting = Clotting(matrix = value["M"], rgb = value["RGB"], defaultRGB = value["DRGB"], brightness = value["Br"], duration = value["duration"], parallelModule = parallelModule)
                    jobDuration = value["duration"] if (value["duration"] != "nil") else None

                    def stop():
                        #clotting.stop()
                        self.setCurrentJobIndex(index = self.currentJobIndex + 1, path = path)

                    self.sequenceManager.schedule(jobDuration, clotting.execute, signalPath = durationPath, onFinish = clotting.stop, updateHandler = stop)
                elif key == "vitalSigns":
                    print("Vital Signs")
                    parallelModule = Module.moduleFromJSON(value["parallelModule"]) if (
                        "parallelModule" in value) else None
                    vitalSigns = VitalSigns(value["duration"], parallelModule)
                    jobDuration = value["duration"] if (value["duration"] != "nil") else None

                    def stop():
                        #delay.stop()
                        self.setCurrentJobIndex(index = self.currentJobIndex + 1, path = path)

                    self.sequenceManager.schedule(jobDuration, vitalSigns.execute, onFinish = vitalSigns.stop, signalPath = durationPath, updateHandler = stop)

        # Reset current job index when starting
        self.setCurrentJobIndex(index = 0, path = path)
        self.sequenceManager.run()
        # When sequence is complete, set current job index to None
        if path is "/main":
            self.setCurrentJobIndex(index = None, path = path)
            database.child("sequence/connected").set(False)

    def start(self):
        # Listen for changes and update local parameters accordingly
        self.sequenceStream = database.child(self.path).stream(self.sync)

synchronizer = SequenceSynchronizer("sequence/main", repeat = True)