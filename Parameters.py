from Authentication import *

class Parameters():

    # Static Properties
    rpm = 0 # pump speed
    Pven = 0.0 # venous pressure
    Part = 0.0 # arterial pressure
    Pint = 0.0
    deltaP = Pint - Part # output - input pressure
    svO2 = 0.0 # oxygen saturation
    Tart = 0.0 # arterial side temperature
    Tven = 0.0 # venous side temperature
    Hb = 0.0 # hemoglobin concentration
    Hct = 0.0 # % volume of red blood cells in body, related to Hb
    V = 0 # flowrate

    def __init__(self):
        print("Parameter object initialized.")
        # TODO: get parameters from Firebase

    def updateParameters(parameters = {}):
        print("Updating parameters:", parameters, "...")
        database.child("parameters").update(parameters)