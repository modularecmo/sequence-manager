import time
import threading
from random import randrange
from Authentication import *

class Parallel:

    def __init__(self, signalPath, completionHandler = None, stopValues = []):
        self.signalPath = signalPath
        self.interventionThread = threading.Thread()
        self.doIContinue = True
        self.signal = None
        self.completionHandler = completionHandler
        self.stopValues = stopValues

    def intervene(self, json = {}):
        global doIContinue
        print('Stepping in', threading.currentThread())
        if json is not None:
            updatePath = json["path"]
            updateValue = json["data"]
            updateEvent = json["event"]
        print(json)
        if updateEvent == "put" and updateValue != "nil": #and updatePath == self.signalPath:
            if self.stopValues:
                for value in self.stopValues:
                    if updateValue == value:
                        print('Stopping...')
                        self.doIContinue = False
                        self.signal = updateValue
                        if self.completionHandler is not None: self.completionHandler()
            else:
                print('Stopping...')
                self.doIContinue = False
                self.signal = updateValue
                if self.completionHandler is not None: self.completionHandler()

    def initiateSignal(self, json):
        self.interventionThread = threading.Thread(target = self.intervene, name = 'intervention_thread_' + str(time.time() * time.time()), args = [json])
        self.interventionThread.start()

    def doWhileWaiting(self, method, delay = None):
        print("Executing method until signal detected...")
        while self.doIContinue:
            if delay != None: time.sleep(delay)
            method()
