# MODULAR ECMO SIMULATOR SEQUENCE MANAGER MODULES
# from Parameters import *
# from SequenceManager import *
# from SequenceSynchronizer import *
# from Main import synchronizer
import signal
import sys
import os

from Setup import *

## Generic Modules


global moduleStatusPath
global patientMasterPath
global patientDiagnosticMasterPath
global heaterMasterPath
global oxygenatorMasterPath
moduleStatusPath = "modules/"
patientMasterPath = "patient/"
patientDiagnosticMasterPath = "patientDiagnostics/"
heaterMasterPath = "heater/"
oxygenatorMasterPath = "oxygenator/"

class Module():
    def __init__(self, parallelModule = None):
        self.parallelModule = parallelModule

    def exit(self):
        # Signalling stop sequence
        print("Exiting program (from Module class)...")
        database.child("sequence/main").set(None)
        database.child("sequence/running").set(False)
        database.child("sequence/currentJobIndex").set(None)
        # Stopping all modules
        print("Stopping all modules...")
        Bleeding().stop()
        OxygenatorNoise().stop()
        PowerDisconnection().stop()
        DisplayMessage().stop()
        LineShattering().stop()
        Deoxygenation().stop()

        # Killing program
        os.kill(os.getpid(), signal.SIGINT)
        os._exit
        sys.exit()


    def execute(self):
        if self.parallelModule is not None:
            print("Executing parallel module")
            self.parallelModule.execute()

    def stop(self):
        if self.parallelModule is not None:
            print("Stopping parallel module...")
            print(self.parallelModule)
            self.parallelModule.stop()
        else:
            print("No parallel module to stop.")

    def checkRunSignal(self):
        signal = database.child("sequence/running").get().val()
        if signal is True or signal is None:
            return True
        else:
            self.exit()

    @staticmethod
    def moduleFromJSON(json):
        print("Parallel:")
        if json is not None:
            for key, value in json.items():
                if key == "stop":
                    print("Stop Simulation")
                    stopSimulation = StopSimulation()
                    return stopSimulation
                elif key == "changeParameters":
                    print("Change Parameters")
                    changeParameters = ChangeParameters(value)
                    return changeParameters
                elif key == "displayMessage":
                    print("Display Message")
                    message = DisplayMessage(value["message"], value["barMessage"], value["alarm"], value["priority"],
                                             value["duration"])
                    jobDuration = value["duration"] if (value["duration"] != "nil") else None
                    return message
                elif key == "delay":
                    print("Delay")
                    delay = Delay(value["duration"])
                    jobDuration = value["duration"] if (value["duration"] != "nil") else None
                    return delay
                elif key == "bleeding":
                    print("Bleeding")
                    bleeding = Bleeding(duration = value["duration"], bleedFlowRate = value["bleedFlow"])
                    jobDuration = value["duration"] if (value["duration"] != "nil") else None
                    return bleeding
                elif key == "lineShattering":
                    print("Line Shattering")
                    shatter = LineShattering(value["duration"])
                    jobDuration = value["duration"] if (value["duration"] != "nil") else None
                    return shatter
                elif key == "powerDisconnection":
                    print("Power Disconnection")
                    off = PowerDisconnection(value["duration"], fullBatteryLife = value["fullBatteryLife"])
                    jobDuration = value["duration"] if (value["duration"] != "nil") else None
                    return off
                elif key == "oxygenatorNoise":
                    print("Oxygenator Noise")
                    air = OxygenatorNoise(value["volume"], value["duration"])
                    jobDuration = value["duration"] if (value["duration"] != "nil") else None
                    return air
                elif key == "deoxygenation":
                    print("Deoxygenation")
                    deox = Deoxygenation(value["duration"])
                    jobDuration = value["duration"] if (value["duration"] != "nil") else None
                    return deox
                elif key == "clotting":
                    print("Clotting")
                    clotting = Clotting(value["M"], value["RGB"], value["DRGB"], value["Br"], value["duration"])
                    jobDuration = value["duration"] if (value["duration"] != "nil") else None
                    return clotting
                elif key == "vitalSigns":
                    print("Vital Signs")
                    vitalSigns = VitalSigns(value["duration"])
                    jobDuration = value["duration"] if (value["duration"] != "nil") else None
                    return vitalSigns

class StopSimulation(Module):
    """Ceases simulation after calling its execute method."""

    def __init__(self):
        super().__init__()
        self.stop = True

    def execute(self):
        if self.checkRunSignal() is True:
            super().execute()
            print("Stopping simulation...")
            self.exit()


class ChangeParameters(Module):
    """Changes a dictionary of ECMO parameters."""

    def __init__(self, parameters = {}, parallelModule = None):
        super().__init__(parallelModule)
        self.parameters = parameters

    def execute(self):
        if self.checkRunSignal() is True:
            super().execute()
            print("Changing parameters...")
            Parameters.updateParameters(self.parameters)


class DisplayMessage(Module):
    """Displays a message/alert on ECMO Machine GUI."""
    # TODO: describe parameter details

    def __init__(self, message = None, barMessage = None, alarm = False, priority = 1, duration = None, parallelModule = None):
        super().__init__(parallelModule)
        self.message = message
        self.barMessage = barMessage
        self.alarm = alarm
        self.priority = priority
        self.duration = duration

    def execute(self):
        if self.checkRunSignal() is True:
            super().execute()
            print("Displaying message...")
            print("Message:", self.message)
            print("Bar Message:", self.barMessage)
            print("Alarm:", self.alarm)
            print("Priority:", self.priority)
            print("Duration:", self.duration)
            messageString = self.message if self.message != "nil" else None
            barMessageString = self.barMessage if self.barMessage != "nil" else None
            database.child(moduleStatusPath + "displayMessage").set({"alarm": self.alarm, "message": messageString, "barMessage": barMessageString, "priority": self.priority})

    def stop(self):
        super().stop()
        print("Stopping display message...")
        database.child(moduleStatusPath + "displayMessage").set(None)

class WaitForAction(Module):
    """Pauses simulation and waits for success/failure intervention to continue. Success intervention branches main queue into a success job and failure intervention bracnhes it into a failure job."""

    def __init__(self, jobID, sequenceSynchronizer, successQueue, failureQueue):
        super().__init__()
        self.jobID = jobID
        self.success = None
        self.sequenceSynchronizer = sequenceSynchronizer
        self.successQueue = successQueue
        self.failureQueue = failureQueue

    def onSuccess(self):
        print("Wait For Action: Success triggered.")
        if self.successQueue is not None:
            path = "/main/" + str(self.jobID) + "/waitForAction/successQueue"
            self.sequenceSynchronizer.createAndRunSequenceFrom(self.successQueue, path)

    def onFailure(self):
        print("Wait For Action: Failure triggered.")
        if self.failureQueue is not None:
            path = "/main/" + str(self.jobID) + "/waitForAction/failureQueue"
            self.sequenceSynchronizer.createAndRunSequenceFrom(self.failureQueue, path)

    def execute(self):
        if self.checkRunSignal() is True:
            super().execute()
            print("Waiting for instructor action...")
            signalPath = "/main/" + str(self.jobID) + "/waitForAction/success"
            self.sequenceSynchronizer.currentJobIndexBackup = self.sequenceSynchronizer.currentJobIndex
            self.sequenceSynchronizer.sequenceManager.waitForAction(onSuccess = self.onSuccess, onFailure = self.onFailure, signalPath = signalPath)

class Delay(Module):
    """Pauses simulation for a specified period of time."""

    def __init__(self, duration, parallelModule = None):
        super().__init__(parallelModule)
        self.duration = duration

    def execute(self):
        if self.checkRunSignal() is True:
            super().execute()
            print("Executing delay for", self.duration, "sec...")

class VitalSigns(Module):
    """Placeholder for vital signs instructions."""

    def __init__(self, duration, parallelModule=None):
        super().__init__(parallelModule)
        self.duration = duration

    def execute(self):
        if self.checkRunSignal() is True:
            super().execute()
            print("Executing vital signs for", self.duration, "sec...")

## Simulation Modules

class Bleeding(Module):
    """Simulates patient bleeding with specified duration."""

    def __init__(self, bleedFlowRate = None, duration = None, parallelModule = None):
        super().__init__(parallelModule)
        self.duration = duration
        self.bleedFlowRate = bleedFlowRate
        self.path = moduleStatusPath + patientMasterPath

    def execute(self):
        if self.checkRunSignal() is True:
            super().execute()
            print("Executing bleeding with flow rate " + str(self.bleedFlowRate) + " and duration " + str(self.duration) + " sec...")
            database.child(self.path + "bleed").set(1)
            database.child(self.path + "bleedLimit").set(self.bleedFlowRate)


    def stop(self):
        super().stop()
        print("Stopping bleeding...")
        database.child(self.path + "bleed").set(0)


class LineShattering(Module):
    """Simulates line shattering bleeding with specified duration."""

    def __init__(self, duration = None, mode = 2, center = False, parallelModule = None):
        super().__init__(parallelModule)
        self.duration = duration
        self.mode = mode
        self.center = center
        self.path = moduleStatusPath + patientMasterPath + "lineShattering"

    def execute(self):
        if self.checkRunSignal() is True:
            super().execute()
            print("Executing line shattering with mode " + str(self.mode) +  " and duration " + str(self.duration) + " sec...")
            database.child(self.path).set(self.mode)

    def stop(self):
        super().stop()
        print("Stopping line shattering...")
        database.child(self.path).set(0)


class PowerDisconnection(Module):
    """Simulates power disconnection with specified duration."""

    def __init__(self, batteryLevel = None, fullBatteryLife = None, duration = None, parallelModule = None):
        super().__init__(parallelModule)
        self.duration = duration
        self.batteryLevel = batteryLevel
        self.fullBatteryLife = fullBatteryLife # in minutes

    def execute(self):
        if self.checkRunSignal() is True:
            super().execute()
            print("Executing power disconnection with duration " + str(self.duration) + " sec and full battery life of " + str(self.fullBatteryLife) + " min...")
            database.child(moduleStatusPath + "powerDisconnection").set({"batteryLevel": self.batteryLevel, "fullBatteryLife": self.fullBatteryLife})

    def stop(self):
        super().stop()
        print("Stopping power disconnection...")
        database.child(moduleStatusPath + "powerDisconnection").set(None)


class Deoxygenation(Module):
    """Simulates deoxygenation with specified duration."""

    def __init__(self, duration=None, parallelModule = None):
        super().__init__(parallelModule)
        self.duration = duration
        self.path = moduleStatusPath + heaterMasterPath + "oxygenation"

    def execute(self):
        if self.checkRunSignal() is True:
            super().execute()
            print("Executing deoxygenation with duration " + str(self.duration) + " sec...")
            database.child(self.path).set(1)

    def stop(self):
        super().stop()
        print("Stopping deoxygenation...")
        database.child(self.path).set(0)


class OxygenatorNoise(Module):
    """Simulates air noise in oxygenator's pump with specified duration."""

    def __init__(self, volume = 1, duration = None, parallelModule = None):
        super().__init__(parallelModule)
        self.volume = volume
        self.duration = duration
        self.path = moduleStatusPath + oxygenatorMasterPath + "noise"

    def execute(self):
        if self.checkRunSignal() is True:
            super().execute()
            print("Executing oxygenator noise with volume " + str(self.volume) + " with duration " + str(self.duration) + " sec...")
            database.child(self.path).set(self.volume)

    def stop(self):
        super().stop()
        print("Stopping oxygenator noise...")
        database.child(self.path).set(0)


class Clotting(Module):
    """Simulates clotting in the oxygenator with specified duration."""

    def __init__(self, matrix = "", rgb = "", defaultRGB = "", brightness = "", duration = None, parallelModule = None):
        super().__init__(parallelModule)
        self.matrix = matrix
        self.rgb = rgb
        self.defaultRGB = defaultRGB
        self.brightness = brightness
        self.duration = duration
        self.path = moduleStatusPath + oxygenatorMasterPath + "clotting"

    def execute(self):
        if self.checkRunSignal() is True:
            super().execute()
            print("Executing clotting with matrix " + self.matrix + ", rgb " + self.rgb + ", default rgb " + self.defaultRGB + ", and brightness " + str(self.brightness) +  ", with duration " + str(self.duration) + " sec...")
            database.child(self.path).set({"M": self.matrix, "RGB": self.rgb, "DRGB": self.defaultRGB, "Br": self.brightness})

    def stop(self):
        super().stop()
        print("Stopping clotting...")
        database.child(self.path).set({"M": "000000000000000000000000000000", "RGB": "0,0,0", "DRGB": self.defaultRGB, "Br": self.brightness})
