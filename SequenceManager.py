# import Parameters
# from threading import Timer
# import sched, time
# from Parallel import *
# from SequenceSynchronizer import *

from Setup import *
import signal
import sys
import os

class SequenceManager():
    # The main scheduler object
    scheduler = None

    # Firebase wait for action stream
    waitForActionStream = None

    # Current total duration of sequence
    currentTotalDuration = 0.0

    def __init__(self):
        self.scheduler = sched.scheduler(time.time, time.sleep)

    # Check whether simulation should continue
    def checkRunSignal(self):

        # Exits the program
        def exit():
            print("Exiting program...")

            global parallel
            if parallel is not None:
                print("Joining other threads...")
                parallel.interventionThread.join()

            # Stop all modules
            from Modules import Module
            Module().exit()

            # Close other threads
            # print("Closing wait for action thread...")
            # self.waitForActionStream.close()

            # Kill program
            sys.exit()
            os._exit
            os.kill(os.getpid(), signal.SIGINT)
            killNumber = 5
            killString = "bye" + killNumber

        signal = database.child("sequence/running").get().val()
        if signal is True or signal is None:
            return True
        else:
            exit()

    # Schedule a function after a specified duration (in seconds)
    # This functions runs schedules sequentially
    # An optional argument array can be passed; which will be the arguments of the onCompletion function
    # Note that argument must be an array
    def schedule(self, duration, action, argument = None, signalPath = None, onFinish = None, updateHandler = None):
        def stop():
            print("Stopping module...")
            if onFinish is not None:
                print("Executing onFinish handler...")
                onFinish()
                if updateHandler is not None:
                    updateHandler()

        def completionHandler(argument = None):
            if argument == None:
                action()
            else:
                action(argument)

            if duration is not None:
                print("Waiting", str(duration), "sec...")
                time.sleep(duration)
                stop()
            else:
                def wait():
                    print("Waiting for intervention from", signalPath)
                    self.checkRunSignal()
                    time.sleep(1)

                print("No duration specified. Waiting till external intervention...")
                parallel = Parallel(signalPath = signalPath, completionHandler = stop)
                streamPath = "sequence/" + signalPath if signalPath is not None else "sequence"
                self.waitForActionStream = database.child(streamPath).stream(parallel.initiateSignal)
                parallel.doWhileWaiting(method = wait)
                self.waitForActionStream.close()
                parallel.interventionThread.join()
                print("Module stopped by external signal.")

        if argument == None:
            self.scheduler.enter(self.currentTotalDuration, 1, completionHandler)
        else:
            self.scheduler.enter(self.currentTotalDuration, 1, completionHandler, argument = argument)

        if duration != None:
            self.currentTotalDuration += duration

    # Change variables by reference
    # variable is an array of parameters
    # values is an array of values to be assigned to variable array according to its order
    def changeVariables(self, variables, values, printLabels = None):
        for (index) in range(len(variables)):
            variables[index] = values[index]

        if printLabels != None:
            for index in range(len(printLabels)):
                print(printLabels[index] + ": " + str(variables[index]))
            print("")
        return variables

    # Changes an array of variables after a specified delay
    def changeVariablesAt(self, duration, variables, values, printLabels = None):
        self.duration = duration
        self.variables = variables
        self.values = values

        if printLabels != None:
            self.schedule(duration = duration, onCompletion = self.changeVariables, argument = [variables, values, printLabels])
        else:
            self.schedule(duration = duration, onCompletion = self.changeVariables, argument = [variables, values])

    # Prompts user to select subsequent action
    # Parameters are onSuccess and onFailure methods
    # If 1 was input, onSuccess will be called, and if 0 was input, onFailure will be called
    def waitForAction(self, onSuccess, onFailure, signalPath):
        global parallel

        def checkStatus(signal):
            print("Instructor action received:", signal)
            if signal is True or signal is False:
                print("Wait for action updated with", signal)
                if signal is True:
                    onSuccess()
                if signal is False:
                    onFailure()
                self.waitForActionStream.close()

        def wait():
            print("Waiting for action...")
            self.checkRunSignal()
            time.sleep(1)

        parallel = Parallel(signalPath = signalPath, stopValues = [True, False])
        self.waitForActionStream = database.child("sequence").stream(parallel.initiateSignal)
        parallel.doWhileWaiting(method = wait)
        checkStatus(parallel.signal)
#       if parallel is not None:
#           parallel.interventionThread.join()

        # self.schedule(duration = 1, onCompletion = waitForAction, argument = [onSuccess, onFailure])

    # Prints a string after a specified number of seconds
    def printAt(self, duration, message):
        self.schedule(duration = duration, onCompletion = print, argument = [message])

    # Run the sequence
    def run(self):
        print("Running sequence...")
        self.scheduler.run()

    # Remove all actions of the current instance
    def stop(self):
        print("Stopping sequence...")
        for event in self.scheduler.queue:
            self.scheduler.cancel(event=event)
        self.scheduler.run()

# Initializing global SequenceManager object
sequenceManager = SequenceManager()